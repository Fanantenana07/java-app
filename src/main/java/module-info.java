module com.example.socket {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;

    opens com.example.socket to javafx.fxml;
    exports com.example.socket;
}