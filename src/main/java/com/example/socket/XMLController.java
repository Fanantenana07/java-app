package com.example.socket;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class XMLController implements Initializable {
    @FXML
    private TableView<XmlTable> XMLTable;
    @FXML
    private TableColumn<XmlTable, String> nomCol;
    @FXML
    private TableColumn<XmlTable, Void> editCol;
    @FXML
    private TableColumn<XmlTable, Void> supprCol;

    //
//    @FXML
//    private TableColumn<Etudiant, String> bourseCol;
//    @FXML
//    private TableColumn<Etudiant, String> adresseCol;
//    @FXML
//    private TableColumn<Etudiant, Void> editCol;
//    @FXML
//    private TableColumn<Etudiant, Void> supprCol;
//    @FXML
//    private TextField search;
    private ObservableList<XmlTable> XmlList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nomCol.setCellValueFactory(new PropertyValueFactory<XmlTable,String>("nom"));
        fileListe();
        XMLTable.setItems(XmlList);

        Callback<TableColumn<XmlTable,Void>, TableCell<XmlTable,Void>> cellfactory = new Callback<TableColumn<XmlTable, Void>, TableCell<XmlTable, Void>>() {
            @Override
            public TableCell<XmlTable, Void> call(TableColumn<XmlTable, Void> xmlVoidTableColumn) {
                final TableCell<XmlTable, Void> cell = new TableCell<>(){
                    private final Button btn = new Button("Modifier");

                    {
                        btn.setTextFill(Color.WHITE);
                        btn.setStyle("-fx-background-color : #f0bc23;-fx-padding:5px 15px");
                        btn.setOnAction((ActionEvent event) -> {
                            System.out.println("dsfjs");
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        editCol.setCellFactory(cellfactory);

    }



    public void fileListe(){
        String currentDirectory = System.getProperty("user.dir");
        String dossier = currentDirectory +"/xmlFile"; // Spécifie le chemin vers le dossier à parcourir
        File directory = new File(dossier);

        // Vérifie si le chemin spécifié correspond à un dossier existant
        if (directory.isDirectory()) {
            // Récupère la liste des fichiers dans le dossier
            File[] files = directory.listFiles();

            List<XmlTable> fileNames = new ArrayList<>();
            for (File file : files) {
                XmlTable table;
                if (file.isFile()) {
                    table = new XmlTable(file.getName());
                    System.out.println(file.getName());
                    XmlList.add(table);

                }
            }

        } else {
            System.out.println("Le chemin spécifié ne correspond pas à un dossier existant.");
        }

    }
}
