package com.example.socket;

public class XmlTable {

        private String nom;

        public XmlTable(String nom) {
            this.nom = nom;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

}
