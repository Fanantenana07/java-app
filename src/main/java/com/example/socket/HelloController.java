package com.example.socket;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
public class HelloController implements Initializable {

    @FXML
    private Pane etudiantPane;
    @FXML
    private Pane XMLPane;
    @FXML
    private Label titleLabel;
    @FXML
    private Label subTitleLabel;


    public void etudiant() {
        titleLabel.setText("Etudiants");
        subTitleLabel.setText("Dashboard/Etudiants");
        setPane(etudiantPane, "Etudiant.fxml");
    }

    public void xmlView() {
        titleLabel.setText("XML");
        subTitleLabel.setText("Dashboard/XML");
        setPane(etudiantPane, "Xml.fxml");
    }

    public void xml() {
        titleLabel.setText("XML");
        subTitleLabel.setText("Dashboard/XML");
        setPane(XMLPane, "Matiere.fxml");
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setPane(etudiantPane, "Etudiant.fxml");
    }

    public void setPane(Pane p, String fxml) {
        try {
            etudiantPane.getChildren().clear();
            XMLPane.getChildren().clear();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(fxml));
            loader.load();
            Node node = (Node) loader.getRoot();
            p.getChildren().add(node);
            p.toFront();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}