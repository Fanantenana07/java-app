package com.example.socket;
import java.io.Serializable;

public class Etudiant implements Serializable {
    private  String matricule;
    private String nom;
    private String bourse;
    private String adresse;

    public Etudiant(String matricule, String nom, String bourse,  String adresse) {
        this.matricule = matricule;
        this.nom = nom;
        this.bourse = bourse;

        this.adresse = adresse;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getBourse() {
        return bourse;
    }

    public void setBourse(String bourse) {
        this.bourse = bourse;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
