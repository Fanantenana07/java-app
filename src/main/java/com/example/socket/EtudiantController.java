package com.example.socket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class EtudiantController extends Thread implements Initializable {


    @FXML
    private TableView<Etudiant> etudiantTable;
    @FXML
    private TableColumn<Etudiant, String> matriculeCol;
    @FXML
    private TableColumn<Etudiant, String> nomCol;

    @FXML
    private TableColumn<Etudiant, String> bourseCol;
    @FXML
    private TableColumn<Etudiant, String> adresseCol;
    @FXML
    private TableColumn<Etudiant, Void> editCol;
    @FXML
    private TableColumn<Etudiant, Void> supprCol;
    @FXML
    private Button btnConnect;
    private ObservableList<Etudiant> etudiantObservableList = FXCollections.observableArrayList();

    private Socket Appsocket = null;
    public static Boolean connect = false;


    public  void isConnected(){
        try {
            Socket socket = new Socket("localhost",9696);
            connect = true;
            Appsocket = socket;
            btnConnect.setStyle("-fx-background-color:green");
            btnConnect.setText("connecter");
            getEtudiant();
        } catch (Exception e) {
            System.out.println(e);
            connect  =false;

        }
    }



    public void addEtudiant (){
        Stage stage = new Stage();
        Parent parent = null;
        FXMLLoader loader = new FXMLLoader();
        try {

            loader.setLocation(getClass().getResource("newStudent.fxml"));
            loader.load();
            parent = loader.getRoot();
            NewEtudiantController controller = loader.getController();
            controller.setSocket(Appsocket);
            controller.setTable(etudiantTable);
            Scene scene = new Scene(parent);
            stage.setScene(scene);
            stage.setTitle("New Student");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        matriculeCol.setCellValueFactory(new PropertyValueFactory<Etudiant,String>("matricule"));
        nomCol.setCellValueFactory(new PropertyValueFactory <Etudiant,String>("nom"));
        adresseCol.setCellValueFactory(new PropertyValueFactory <Etudiant,String>("adresse"));
        bourseCol.setCellValueFactory(new PropertyValueFactory <Etudiant,String>("bourse"));
        etudiantTable.setItems(etudiantObservableList);

        Callback<TableColumn<Etudiant,Void>, TableCell<Etudiant,Void>> cellfactory = new Callback<TableColumn<Etudiant, Void>, TableCell<Etudiant, Void>>() {
            @Override
            public TableCell<Etudiant, Void> call(TableColumn<Etudiant, Void> xmlVoidTableColumn) {
                final TableCell<Etudiant, Void> cell = new TableCell<>(){
                    private final Button btn = new Button("Modifier");

                    {
                        btn.setTextFill(Color.WHITE);
                        btn.setStyle("-fx-background-color : #f0bc23;-fx-padding:5px 15px");
                        btn.setOnAction((ActionEvent event) -> {
                            System.out.println("dsfjs");
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        editCol.setCellFactory(cellfactory);

    }

    public void getEtudiant() throws IOException, ClassNotFoundException {
        if( Appsocket != null){
            Class.forName("com.example.socket.Etudiant");
            OutputStream outputStream = Appsocket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(outputStream, true);
            printWriter.println("GET Etudiant");
            ObjectInputStream objectInputStream = new ObjectInputStream(Appsocket.getInputStream());
            List<Etudiant> etudiants = (List<Etudiant>) objectInputStream.readObject();
            System.out.println(etudiants.size());
//            for (Etudiant etudiant : etudiants) {
//                Etudiant etudiant1;
//                etudiant1 = new Etudiant(etudiant.getMatricule(),etudiant.getNom(),etudiant.getAdresse(),etudiant.getMatricule());
//                etudiantObservableList.add(etudiant1);
//            }
        }
        else {
            System.out.println("not connected");
        }


    }


}
