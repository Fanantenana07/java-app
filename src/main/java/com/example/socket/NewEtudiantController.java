package com.example.socket;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.Socket;
import java.time.LocalDateTime;

public class NewEtudiantController {

    @FXML
    private TextField matricule,nom,adresse,bourse;

    @FXML
    private AnchorPane pane;


    private TableView<Etudiant> tableView;
    private Socket Etudiantsocket;
    public void setTable (TableView<Etudiant> t){
        tableView = t;
    }

    public  void setSocket (Socket socket){
        Etudiantsocket = socket;
    }
    public void close (){
        Stage stage = (Stage) pane.getScene().getWindow();
        stage.close();
    }
    public void addEtudiant () throws IOException {
        String matriculeV = matricule.getText();
        String nomV = nom.getText();
        String adresseV = adresse.getText();
        String bourseV = bourse.getText();
        if ((matriculeV == "") ||
                (nomV == "") ||
                (adresseV =="")||
                (bourseV =="")
        ){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Attention");
            alert.setContentText("Un champs du formulaire est vide. Veillez le remplir");
            alert.show();
        } else {
            if(Etudiantsocket!=null && Etudiantsocket.isConnected()){
                InputStream inputStream = Etudiantsocket.getInputStream();
                OutputStream outputStream = Etudiantsocket.getOutputStream();
                PrintWriter printWriter = new PrintWriter(outputStream, true);
                String request = "INSERT INTO etudiant (matricule, nom, adresse, bourse) VALUES ('"+matriculeV+"', '"+nomV+"', '"+adresseV+"', '"+bourseV+"')";
                System.out.println(request);
                printWriter.println(request);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String reponse = bufferedReader.readLine();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setContentText(reponse);
                alert.show();

            }
            else {
                try {
                    // Création du document XML
                    LocalDateTime currentDateTime = LocalDateTime.now();
                    String filename = currentDateTime.toString().replace("-", "")
                            .replace(":", "")
                            .replace(".", "");
                    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                    Document doc = docBuilder.newDocument();

                    // Création de l'élément racine
                    Element racine = doc.createElement("Etudiant");
                    doc.appendChild(racine);
                    Element actionElement = doc.createElement("Action");
                    actionElement.appendChild(doc.createTextNode("INSERT"));
                    racine.appendChild(actionElement);

                    Element matriculeElement = doc.createElement("Matricule");
                    matriculeElement.appendChild(doc.createTextNode(matriculeV));
                    racine.appendChild(matriculeElement);

                    Element nomElement = doc.createElement("Nom");
                    nomElement.appendChild(doc.createTextNode(nomV));
                    racine.appendChild(nomElement);

                    Element adresseElement = doc.createElement("adresse");
                    adresseElement.appendChild(doc.createTextNode(adresseV));
                    racine.appendChild(adresseElement);

                    Element bourseElement = doc.createElement("bourse");
                    bourseElement.appendChild(doc.createTextNode(bourseV));
                    racine.appendChild(bourseElement);

                    // Génération du fichier XML
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    transformer.transform(new DOMSource(doc), new StreamResult(new File("xmlFile/"+filename+".xml")));
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setContentText("Fichier enregistrer avec success.");
                    alert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }

    }
}
